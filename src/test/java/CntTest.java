package com.qaagility.controller;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.Test;
import org.mockito.Mockito;

public class CntTest extends Mockito{

    @Test
    public void testCnt() throws Exception {

        int k= new Cnt().d(5,5);
        assertEquals("Problem with Cnt function:", 1, k);
        
    }
}
